FROM alpine:3.21.3

# renovate: datasource=repology depName=alpine_3_21/tor versioning=loose
ENV TOR_VERSION="0.4.8.14-r1"

# renovate: datasource=repology depName=alpine_3_21/dumb-init versioning=loose
ENV DUMB_INIT_VERSION="1.2.5-r3"

RUN set -feux \
  && apk --no-cache add tor=${TOR_VERSION} \
                        dumb-init=${DUMB_INIT_VERSION} \
  && mv /etc/tor/torrc.sample /etc/tor/torrc \
  && sed -i -e 's/#SOCKSPort 9050/SOCKSPort 0.0.0.0:9050/g' \
            -e 's@^Log notice file /var/log/tor/notices.log@Log notice stdout@' /etc/tor/torrc

USER tor

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/bin/tor"]
